﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalAttenuator
{
   public  class CalculateManager
    {

        public CalculateManager() {}

        public int Frequency(string second)
        {
            double seconds;
            int freq;

            if (second == "") return -1;
            else
            {
                seconds = double.Parse(second);
                seconds *= 1000;
                freq = (int)seconds;
                if (freq > 32768)
                {
                    MsgBox.EmptyFreq();
                    return -1;
                }
                else
                    return freq;
            }
        }


        public double Two_User(string changing)
        {
            double two_change, change;
            change = double.Parse(changing);
            two_change = 2 * change;
            if (two_change % 2 >=0 ) //בדיקת תקינות קלט
                return two_change;
            else
                return 0;
        }


        public double checking(string PlusMinus ,double check, double change)
        {
            if (PlusMinus=="minus")
                check -= change;
            else if(PlusMinus=="plus")
                check += change;
            return check;
        }
      
        public double Sensitivity(double[] sensitivitytest)
        {
            // sensitivitytest[0] = attenuate of the digital attenuator
            // sensitivitytest[1] = the power of the transsmision
            // sensitivitytest[2]... = attenuate of cables and constats attenuators
            double sensitivity_result;

            sensitivity_result = sensitivitytest[1] - sensitivitytest[0];
            for (int i=2; i< sensitivitytest.Length; i++)
            {
                if (sensitivity_result - sensitivitytest[i] > double.MinValue)
                    sensitivity_result -= sensitivitytest[i];
                else
                {
                    MsgBox.BigAttenuator();
                    return -1;
                }
            }
            return sensitivity_result; 
        }
    }
}
