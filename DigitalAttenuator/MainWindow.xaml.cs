﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalAttenuator;
using System.Windows.Forms;
//using Word = Microsoft.Office.Interop.Word;

// הפרוייקט נקרא גם
//Auto_Attenuator 
//בגירסא הראשונה שלו וגירסאותיו הראשונות מתועדות ב
// bitbucket

namespace DigitalAttenuator
{
    // <summary>
    // Interaction logic for MainWindow.xaml
    // </summary>

    public partial class MainWindow : Window
    {
        CalculateManager CM = new CalculateManager();
        PingManager PM = new PingManager();
        RS232Manager RM = new RS232Manager();
        double sum = 0, checking_att, attstart = 3, attnow;
        bool withping = true, autorise = true, exit = false, middle_autorising = false, on_click;
        int ConstAttenuators = 1;
        string serialstring;


        public MainWindow()
        {
            InitializeComponent();
            DirectReportFolder.Text = DigitalAttenuator.Properties.Settings.Default.DirectReport;
            PortNames();
            for (int i = 0; i < 3; i++)
            {
                AddingAttenuator_Click(null, null);
            }


        }

        private async void AutoRisingButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (Rise_After_Max())
                {
                    if (CM.Frequency(AutoRisingTxt.Text) != -1)
                    {
                        middle_autorising = true;
                        exit = false;
                        for (; sum < Constants.max_attenuate;)
                        {
                            if (exit == false)
                            {
                                if (ReciverCheck())
                                {
                                    sum = RM.HalfPlus();
                                    checking_att = CM.checking("plus", checking_att, 0.5);
                                    if (checking_att == sum)
                                    {
                                        attnow = sum + attstart;
                                        AttenuateNow.Text = attnow.ToString();
                                        MaxAtt(sum);
                                    }
                                    else
                                    {
                                        MsgBox.Problem();
                                    }
                                    await Task.Delay(CM.Frequency(AutoRisingTxt.Text));
                                }
                            }
                            else
                                break;
                        }

                    }
                    Off_Click();
                    middle_autorising = false;
                }

            }


        }

        private void UserPlusButton_Click(object sender, RoutedEventArgs e)
        {

            if (!On_Click())
            {
                if (Rise_After_Max())
                {
                    if (UserControlTxt.Text != "")
                    {
                        if (CM.Two_User(UserControlTxt.Text) == 0)
                            UserChange_NotOK();
                        else
                        {

                            if (Rise_Or_Down_Limits(sum, 1, UserControlTxt.Text))
                            {

                                if (ReciverCheck())
                                {
                                    sum = RM.SendUserChanges(1, CM.Two_User(UserControlTxt.Text));
                                    checking_att = CM.checking("plus", checking_att, double.Parse(UserControlTxt.Text));

                                    if (checking_att == sum)
                                    {
                                        attnow = sum + attstart;
                                        AttenuateNow.Text = attnow.ToString();
                                    }
                                    else
                                    {
                                        MsgBox.Problem();
                                    }
                                    MaxAtt(sum);

                                }
                            }
                            else
                                MsgBox.Changing_Limit();
                        }

                    }
                    else
                        UserChange_NotOK();
                }
                Off_Click();
            }
        }

        private void HalfPlusButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (Rise_After_Max())
                {

                    if (ReciverCheck())
                    {
                        sum = RM.HalfPlus();
                        checking_att = CM.checking("plus", checking_att, 0.5);
                        if (checking_att == sum)
                        {
                            attnow = sum + attstart;
                            AttenuateNow.Text = attnow.ToString();
                        }
                        else
                        {
                            MsgBox.Problem();
                        }
                        MaxAtt(sum);

                    }
                }
                Off_Click();
            }
        }

        private async void UserMinusButton_Click(object sender, RoutedEventArgs e)
        {

            if (!On_Click())
            {
                if (sum > 0)
                {
                    if (UserControlTxt.Text != "")
                    {

                        if (CM.Two_User(UserControlTxt.Text) == 0)
                            UserChange_NotOK();
                        else
                        {

                            if (Rise_Or_Down_Limits(sum, 0, UserControlTxt.Text))
                            {

                                byte[] downing = new byte[] { (byte)CM.Two_User(UserControlTxt.Text) };

                                RM.SendZero();
                                await Task.Delay(50);
                                RM.SendOne();
                                await Task.Delay(50);
                                RM.UserMinus(downing);

                                sum = RM.ReadRS232();
                                checking_att = CM.checking("minus", checking_att, double.Parse(UserControlTxt.Text));
                                if (checking_att == sum)
                                {
                                    attnow = sum + attstart;
                                    AttenuateNow.Text = attnow.ToString();
                                }
                                else
                                {
                                    MsgBox.Problem();
                                }
                                MaxAtt(sum);


                            }
                            else
                                MsgBox.Changing_Limit();
                        }
                    }

                }
                Off_Click();
            }
        }



        private async void ZeroButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                RM.SendZero();
                await Task.Delay(50);
                RM.SendZero();
                await Task.Delay(50);
                RM.SendZero();

                sum = RM.ReadRS232();
                checking_att = 0;
                if (checking_att == sum)
                {
                    attnow = sum + attstart;
                    AttenuateNow.Text = attnow.ToString();
                }
                else
                {
                    MsgBox.Problem();
                }

                MaxAtt(sum);


                Off_Click();
            }
        }

        private async void HalfMinusButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (sum > 0)
                {
                    RM.SendZero();
                    await Task.Delay(50);
                    RM.SendOne();
                    await Task.Delay(50);
                    RM.SendOne();
                    sum = RM.ReadRS232();
                    checking_att = CM.checking("minus", checking_att, 0.5);
                    if (checking_att == sum)
                    {
                        attnow = sum + attstart;
                        AttenuateNow.Text = attnow.ToString();
                    }
                    else
                    {
                        MsgBox.Problem();
                    }
                    MaxAtt(sum);

                }
                Off_Click();
            }
        }






        public bool ReciverCheck()
        {
            int reciver = 0;

            reciver = PM.Recivercheck(withping, IpReciverText.Text);
            if (reciver == -1)
            {
                WithOrWithoutButton.IsChecked = true;
                WithOrWithoutButton_Click(null, null);
                return true;
            }
            else if (reciver == -2)
            {
                IpReciverText.Focus();
                return false;
            }
            else if (reciver == 0)
            {
                if (CM.Sensitivity(Sensitivity_Array()) != -1)
                    SensitiveText.Text = CM.Sensitivity(Sensitivity_Array()).ToString();
                return false;
            }
            else
                return true;

        }

        private void AutoexitButton_Click(object sender, RoutedEventArgs e)
        {
            exit = true;
            middle_autorising = false;
            Off_Click();
        }

        private void sendingPing_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                bool ping_reply;


                ping_reply = PM.OnePing(IpReciverText.Text);
                if (ping_reply)
                {
                    IPTrue.Visibility = Visibility.Visible;
                    IPFalse.Visibility = Visibility.Hidden;
                }
                else
                {
                    IPFalse.Visibility = Visibility.Visible;
                    IPTrue.Visibility = Visibility.Hidden;
                }
                Off_Click();
            }


        }

        public void WithOrWithoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (withping == true)
            {
                withping = false;
                sendingPing.IsEnabled = false;
                IpReciverText.IsEnabled = false;
                IPTrue.Visibility = Visibility.Hidden;
                IPFalse.Visibility = Visibility.Hidden;

                Thread.Sleep(100);


            }
            else
            {
                withping = true;
                sendingPing.IsEnabled = true;
                IpReciverText.IsEnabled = true;

            }
        }

        public void Auto_Manual(bool autoenable, bool manual)
        {

            AutoRisingButton.IsEnabled = autoenable;
            AutoRisingTxt.IsEnabled = autoenable;
            AutoexitButton.IsEnabled = autoenable;
            HalfPlusButton.IsEnabled = manual;
            HalfMinusButton.IsEnabled = manual;
            UserControlTxt.IsEnabled = manual;
            UserMinusButton.IsEnabled = manual;
            UserPlusButton.IsEnabled = manual;
        }

        private void CheckNumInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void EnableAutoRising_Click(object sender, RoutedEventArgs e)
        {
            if (autorise == true)
            {
                autorise = false;
                Auto_Manual(false, true);
                exit = true;

            }
            else
            {
                autorise = true;
                Auto_Manual(true, false);
            }
        }

        public void MaxAtt(double max)
        {
            if (max == Constants.max_attenuate)
            {
                MaxAttenuate.Visibility = Visibility.Visible;
                MinAttenuate.Visibility = Visibility.Hidden;
                HalfPlusButton.IsEnabled = false;
                UserPlusButton.IsEnabled = false;
                exit = true;

            }
            else if (max == 0)
            {
                MinAttenuate.Visibility = Visibility.Visible;
                MaxAttenuate.Visibility = Visibility.Hidden;
            }
            else
            {
                MaxAttenuate.Visibility = Visibility.Hidden;
                MinAttenuate.Visibility = Visibility.Hidden;
                if (middle_autorising == false)
                {
                    HalfPlusButton.IsEnabled = true;
                    UserPlusButton.IsEnabled = true;
                }
            }

        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            double first = -1;

            if (PortNameComboBox.Text != "")
            {

                first = CheckingConnection();
                if (first >= 0)
                {
                    ConnectButton.IsEnabled = false;
                    DisconnectIcon.Visibility = Visibility.Hidden;
                    ConnectIcon.Visibility = Visibility.Visible;

                    PortNameComboBox.IsEnabled = false;
                    sum = RM.ReadRS232();
                    checking_att = sum;
                    attnow = sum + attstart;
                    AttenuateNow.Text = attnow.ToString();
                    MaxAtt(sum);
                    On_Connection(true);

                }
                else
                {
                    DisconnectIcon.Visibility = Visibility.Visible;
                    return;
                }
            }
            else
            {
                DisconnectIcon.Visibility = Visibility.Visible;
                return;
            }


        }

        public async void UserChange_NotOK()
        {
            UserCtrlFalse.Visibility = Visibility.Visible;
            UserControlTxt.Text = "";
            UserControlTxt.Focus();
            await Task.Delay(1000);
            UserCtrlFalse.Visibility = Visibility.Hidden;
        }



        private async void Export_Click(object sender, RoutedEventArgs e)
        {

            if (NamePro.Text == "")
            {
                MsgBox.NoName();
                NamePro.Focus();
                return;
            }

            int i = 1;
            if (DirectReportFolder.Text == "")
            {
                DirectReportFolder.Focus();
                return;
            }
            else
                System.IO.Directory.CreateDirectory(DirectReportFolder.Text);

            if (NamePro.Text != "")
            {
                try
                {
                    //    String str = NamePro.Text;
                    //    NamePro.Text = str.Replace("\"", "''");

                    //    // Get the Word application object.
                    //    Word._Application word_app = new Word.Application();

                    //    // Make Word visible (optional).
                    //    word_app.Visible = true;

                    //    // Create the Word document.
                    //    object missing = Type.Missing;
                    //    Word._Document word_doc = word_app.Documents.Add(
                    //        ref missing, ref missing, ref missing, ref missing);

                    //    // Create a header paragraph.
                    //    Word.Paragraph para = word_doc.Paragraphs.Add(ref missing);
                    //    para.Range.Text = "ניסוי רגישות קליטה";

                    //    object style_name = "כותרת 1";
                    //    para.Range.set_Style(style_name);
                    //    para.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    //    para.Range.InsertParagraphAfter();
                    //    string old_font = para.Range.Font.Name;
                    //    para.Range.Font.Name = "David";

                    //    // Add more text.
                    //    para.Range.Text = "עוצמת השידור היא " + " : " + " dB" + PowerTranssmit.Text;
                    //    para.Range.InsertParagraphAfter();

                    //    while (i < ConstAttenuators)
                    //    {
                    //        string textboxname = "AttenuatorValue" + i.ToString();
                    //        string comboboxname = "Attenuators" + i.ToString();
                    //        System.Windows.Controls.TextBox AttenuatorValue = this.AttenuatorsBoxGrid.FindName(textboxname) as System.Windows.Controls.TextBox;
                    //        System.Windows.Controls.ComboBox AttenuatorsText = this.AttenuatorsTextGrid.FindName(comboboxname) as System.Windows.Controls.ComboBox;
                    //        if (AttenuatorValue.Text != "")
                    //        {
                    //            para.Range.Text = AttenuatorsText.Text + " : " + " dB" + AttenuatorValue.Text;
                    //            para.Range.InsertParagraphAfter();
                    //        }
                    //        i++;
                    //    }

                    //    // Start a new paragraph and then
                    //    // switch back to the original font.
                    //    para.Range.InsertParagraphAfter();
                    //    para.Range.Text = "רגישות הקליטה היא " + " : " + " dB" + SensitiveText.Text;
                    //    para.Range.InsertParagraphAfter();
                    //    para.Range.Font.Name = old_font;

                    //    await Task.Delay(1000);
                    //    // Save the document.
                    //string address = DirectReportFolder.Text + "\\" + NamePro.Text + " " + DateTime.Today.ToString("dd.MM.yyyy") + ".doc";
                    //object filename = address;
                    //    word_doc.SaveAs(ref filename, ref missing, ref missing,
                    //        ref missing, ref missing, ref missing, ref missing,
                    //        ref missing, ref missing, ref missing, ref missing,
                    //        ref missing, ref missing, ref missing, ref missing,
                    //        ref missing);

                    //    // Close.
                    //    //object save_changes = false;
                    //    //word_doc.Close(ref save_changes, ref missing, ref missing);
                    //    //word_app.Quit(ref save_changes, ref missing, ref missing);
                }
                catch { }
            }

        }

        private void BrowserButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            DirectReportFolder.Text = dialog.SelectedPath;
            DigitalAttenuator.Properties.Settings.Default.DirectReport = DirectReportFolder.Text;
            DigitalAttenuator.Properties.Settings.Default.Save();
        }

        private void AddingAttenuator_Click(object sender, RoutedEventArgs e)
        {
            //הוספת מנחתים
            AttenuatorsTextGrid.RowDefinitions.Add(new RowDefinition());
            System.Windows.Controls.ComboBox attenuators_or_cables = new System.Windows.Controls.ComboBox();
            attenuators_or_cables.Name = "Attenuators" + ConstAttenuators.ToString();
            attenuators_or_cables.Items.Add("מנחת");
            attenuators_or_cables.Items.Add("רתמה");
            attenuators_or_cables.VerticalAlignment = VerticalAlignment.Center;
            attenuators_or_cables.FontFamily = new FontFamily("David");
            attenuators_or_cables.FontSize = 16;
            attenuators_or_cables.FlowDirection = System.Windows.FlowDirection.RightToLeft;
            AttenuatorsTextGrid.Children.Add(attenuators_or_cables);
            AttenuatorsTextGrid.RegisterName(attenuators_or_cables.Name, attenuators_or_cables);
            attenuators_or_cables.SetValue(Grid.RowProperty, ConstAttenuators - 1);


            //הוספת תיבות טקסט

            AttenuatorsBoxGrid.RowDefinitions.Add(new RowDefinition());
            System.Windows.Controls.TextBox AttenuatorValue = new System.Windows.Controls.TextBox();
            AttenuatorValue.Name = "AttenuatorValue" + ConstAttenuators.ToString();
            AttenuatorValue.BorderBrush = new SolidColorBrush(Colors.Black);
            AttenuatorValue.VerticalAlignment = VerticalAlignment.Center;
            AttenuatorValue.MaxLength = 15;
            AttenuatorValue.PreviewTextInput += CheckNumInput;
            AttenuatorValue.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            AttenuatorValue.FontFamily = new FontFamily("David");


            AttenuatorsBoxGrid.Children.Add(AttenuatorValue);

            AttenuatorsBoxGrid.RegisterName(AttenuatorValue.Name, AttenuatorValue);
            AttenuatorValue.SetValue(Grid.RowProperty, ConstAttenuators - 1);
            ConstAttenuators++;



        }

        public bool On_Click()
        {
            if (on_click)
                return true;
            else
            {
                on_click = true;
                return false;
            }
        }




        private void PortNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool CheckRMinit;

            DisconnectIcon.Visibility = Visibility.Hidden;
            ConnectIcon.Visibility = Visibility.Hidden;

            RM.ClosePort();
            CheckRMinit = RM.init(PortNameComboBox.Text);
            if (CheckRMinit)
            {
                RM.OpenPort();

                double first = -1;

                if (PortNameComboBox.Text != "")
                {

                    first = CheckingConnection();
                    if (first >= 0)
                    {
                        ConnectButton.IsEnabled = false;
                        DisconnectIcon.Visibility = Visibility.Hidden;
                        ConnectIcon.Visibility = Visibility.Visible;

                        PortNameComboBox.IsEnabled = false;
                        sum = RM.ReadRS232();
                        checking_att = sum;
                        attnow = sum + attstart;
                        AttenuateNow.Text = attnow.ToString();
                        MaxAtt(sum);
                        On_Connection(true);

                    }
                    else
                    {
                        DisconnectIcon.Visibility = Visibility.Visible;
                        return;
                    }
                }
                else
                {
                    DisconnectIcon.Visibility = Visibility.Visible;
                    return;
                }
            }
            else
            {
                MsgBox.Serialconnection();
            }


        }

        public void Off_Click()
        {
            on_click = false;
        }

        public bool Rise_After_Max()
        {
            if (sum < Constants.max_attenuate)
                return true;
            else
                return false;
        }

        public bool Rise_Or_Down_Limits(double sum, int PlusOrMinus, string UserChange)
        {
            double UserChanges;
            UserChanges = double.Parse(UserChange);
            if (PlusOrMinus == 1)
            {
                if (sum + UserChanges <= Constants.max_attenuate) return true;
                else return false;

            }
            else
            {
                if (sum - UserChanges >= 0) return true;
                else return false;
            }
        }

        public void PortNames()
        {
            string[] portsName = RM.GetportNames();
            foreach (string portname in portsName)
            {
                PortNameComboBox.Items.Add(portname);
            }
        }

        public double CheckingConnection()
        {
            double first = -1;
            int timer = 0;


            Task read = Task.Factory.StartNew(() =>
             {
                 first = RM.ReadRS232();

             });
            Task<double> time = Task<double>.Factory.StartNew(() =>
            {
                while (true)
                {
                    timer++;
                    Thread.Sleep(1000);
                    if (first >= 0)
                        return first;
                    if (timer == 1)
                    {
                        first = -2;
                        return first;
                    }

                }
            });
            if (time.Result == -2)
                return -2;
            else
                return first;




        }

        public void On_Connection(bool enable)
        {
            WithOrWithoutButton.IsEnabled = enable;
            IpReciverText.IsEnabled = enable;
            sendingPing.IsEnabled = enable;
            EnableAutoRising.IsEnabled = enable;
            AutoRisingTxt.IsEnabled = enable;
            AutoRisingButton.IsEnabled = enable;
            AutoexitButton.IsEnabled = enable;
            ZeroButton.IsEnabled = enable;
        }

        public double[] Sensitivity_Array()
        {
            int arraysize = 2, k = 2;
            for (int i = 1; i < ConstAttenuators; i++)
            {
                string textboxname = "AttenuatorValue" + (i).ToString();
                System.Windows.Controls.TextBox AttenuatorValue = this.AttenuatorsBoxGrid.FindName(textboxname) as System.Windows.Controls.TextBox;
                if (AttenuatorValue.Text != "")
                {
                    arraysize++;

                }
            }
            double[] sensitivity = new double[arraysize];
            sensitivity[0] = attnow;
            if (PowerTranssmit.Text != "")
                sensitivity[1] = double.Parse(PowerTranssmit.Text);
            else
                PowerTranssmit.Focus();





            for (int j = 1; j < ConstAttenuators; j++)
            {
                string textboxname = "AttenuatorValue" + (j).ToString();
                System.Windows.Controls.TextBox AttenuatorValue = this.AttenuatorsBoxGrid.FindName(textboxname) as System.Windows.Controls.TextBox;
                if (AttenuatorValue.Text != "")
                {
                    sensitivity[k] = double.Parse(AttenuatorValue.Text);
                    k++;
                }


            }

            return sensitivity;
        }

    }
}



