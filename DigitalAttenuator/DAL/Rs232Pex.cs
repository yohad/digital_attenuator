﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO.Ports;

namespace DigitalAttenuator
{
    class Rs232Pex
    {
        private string portName;
        private SerialPort port;


        public Rs232Pex(string portsname)
        {
            portName = portsname;
            port = new SerialPort(portName, 9600);
        }


        public void OpenPort()
        {
            try
            {
                port.Open();
            }
            catch { }
        }

        public void ClosePort()
        {
            port.Close();
            
        }

        public void SendingCommand(byte[] changeattenuate)
        {
            port.Write(changeattenuate, 0, changeattenuate.Length);
        }
        

        public double ReadNow()
        {
            try
            {
                double portnow;
                portnow = (double)port.ReadChar();
                return portnow;
            }
            catch { return -1; }
        }

        public string[] GetPortNames()
        {
            string[] portnames = SerialPort.GetPortNames();
            return portnames;
        }

    }
}
