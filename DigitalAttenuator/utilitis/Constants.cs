﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalAttenuator
{
    static class Constants
    {
        public const int number_pings=10;
        public const double max_attenuate = 31.5;
    }
}
