﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DigitalAttenuator
{
    public static class MsgBox
    { 
        public static void BigAttenuator()
        {
            MessageBox.Show("הערכים של המנחתים גדולים מידי");
        }
       
        public static void NoName()
        {
            MessageBox.Show("יש לבחור שם לפרוייקט");
        }
    
        public static void EmptyFreq()
        {
            MessageBox.Show("יש לבחור תדירות בתחום");
        }

        public static void Problem()
        {
            MessageBox.Show("יש בעיה עם הקריאה או עם הכתיבה למעגל הערך שהוא מחזיר אינו הערך הנדרש, אם כיבית את המעגל הפעל מחדש את התוכנה");
        }

        public static void Changing_Limit()
        {
            MessageBox.Show("המספר שאתה מבקש אינו בתחום");
        }

        public static void Serialconnection()
        {
            MessageBox.Show("נסה שנית");
        }
    }
}
